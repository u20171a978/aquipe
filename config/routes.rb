Rails.application.routes.draw do
  #get 'pages/busqueda'
  #get 'busqueda', controller: :pages, action: :busqueda, alias: 'busqueda'
  #get 'registrate', controller: :pages, action: :registrate, alias: 'registrate'
  resources :users
  resources :parkings
  resources :home
  #resources :searches
=begin
    get "/users" index
    post "/users" create
    delete "/users" delete
    get "/users/:id" show
    get "/users/new" new
    get "/users/:id/edit" edit
    patch "users/:id" update
    put "users/:id" update
=end

  get :search, controller: :home

 
  #post 'user' => "pages#create" #Se envia los datos por post desde la vista registrate.html.erb
  get 'registro'   => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  get 'actualizar' => 'pages#actualizar'

  root 'home#index'
  #get 'mis-estacionamiento', controller: :pages, action: :misestacionamiento, alias: 'mis-estacionamiento'
  #get 'reservacion', controller: :pages, action: :reservacion, alias: 'reservacion'
  #get 'cuenta', controller: :pages, action: :cuenta, alias: 'cuenta'
  get 'mis-reservaciones', controller: :pages, action: :misreservaciones, alias: 'mis-reservaciones'
  #get 'crear-estacionamiento', controller: :pages, action: :crearestacionamiento, alias: 'crear-estacionamiento'
  get 'ticket', controller: :pages, action: :ticket, alias: 'ticket'
  get 'actualizar', controller: :pages, action: :actualizar, alias: 'actualizar'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  
end
