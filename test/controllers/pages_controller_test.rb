require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get busqueda" do
    get pages_busqueda_url
    assert_response :success
  end

  test "should get registrate" do
    get pages_registrate_url
    assert_response :success
  end

  test "should get sesion" do
    get pages_sesion_url
    assert_response :success
  end

  test "should get misestacionamiento" do
    get pages_misestacionamiento_url
    assert_response :success
  end

  test "should get reservacion" do
    get pages_reservacion_url
    assert_response :success
  end

  test "should get cuenta" do
    get pages_cuenta_url
    assert_response :success
  end

  test "should get misreservaciones" do
    get pages_misreservaciones_url
    assert_response :success
  end

  test "should get crearestacionamiento" do
    get pages_crearestacionamiento_url
    assert_response :success
  end

  test "should get ticket" do
    get pages_ticket_url
    assert_response :success
  end

  test "should get actualizar" do
    get pages_actualizar_url
    assert_response :success
  end

end
