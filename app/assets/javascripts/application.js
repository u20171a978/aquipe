// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .



function loopingClouds() {
    var imageSize;
    if(window.innerWidth >= 1024) {
      imageSize = 3229; 
    } else {
      imageSize = 1536; 
    }
  
    var x = 0;
    var y = imageSize;
    var a = 0;
    var b = imageSize;
    var value;
  
    if(window.innerWidth >= 1024) {
      value = 0.13;
    } else if(window.innerWidth >= 768) {
      value = 0.15;
    } else {
      value = 0.1;
    }
    setInterval(function() {
      x-=value;
      y-=value;
      a-=0.2;
      b-=0.2;
  
      $('.cloud-cluster .background .before').css({'transform' : 'translateX(' + x +'px)'});
      $('.cloud-cluster .background .after').css({'transform' : 'translateX(' + y +'px)'});
      $('.cloud-cluster .foreground .before').css({'transform' : 'translateX(' + a +'px)'});
      $('.cloud-cluster .foreground .after').css({'transform' : 'translateX(' + b +'px)'});
  
      if(x.toString().slice(1) >= imageSize) {
        x = (imageSize);
      }
      if(y.toString().slice(1) >= imageSize) {
        y = (imageSize - 5);
      }
      if(a.toString().slice(1) >= imageSize) {
        a = (imageSize - 5);
      }
      if(b.toString().slice(1) >= imageSize) {
        b = (imageSize - 5);
      }
    }, 10);
}
loopingClouds();


