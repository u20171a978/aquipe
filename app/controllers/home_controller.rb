class HomeController < ApplicationController
  http_basic_authenticate_with name: "mlagos@gmail.com", password: "123456", except: [:index, :show, :search]
  def index
  end
  def search
    search = params[:home][:search]
    logger.debug "Búsqueda: #{search}"
    if params[:home][:search].blank?
        @parkings = Parking.all
    else
        @parkings = Parking.search(search)
    end
  end
end
