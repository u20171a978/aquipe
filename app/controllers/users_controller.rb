class UsersController < ApplicationController
    def index      
    end    
    def new
        @user = User.new
    end
    def create
        @user = User.new(
            first_name: params[:users][:first_name],
            last_name: params[:users][:last_name],
            type_document: params[:users][:type_document],
            n_document: params[:users][:n_document],
            f_birth: params[:users][:f_birth],
            phone: params[:users][:phone],
            sex: params[:users][:sex],
            email: params[:users][:email],
            password: params[:users][:password],
            remember_token: params[:users][:remember_token],
            tyc: params[:users][:tyc]
            )
        if @user.save
            redirect_to @home
        else
            render :new
        end
    end
    
end