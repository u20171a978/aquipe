class ParkingsController < ApplicationController
    def index      
    end 
    def new
    end
    def show
        @parking = Parking.find(params[:id])
    end
    def create
        @parking = Parking.new(
            name: params[:parkings][:name],
            address: params[:parkings][:address],
            district: params[:parkings][:district],
            google_maps: params[:parkings][:google_maps],
            phone: params[:parkings][:phone],
            price: params[:parkings][:price],
            pkg_location: params[:parkings][:pkg_location],
            pkg_place: params[:parkings][:pkg_place],
            quantity: params[:parkings][:quantity],
            service_wash: params[:parkings][:service_wash],
            service_rest: params[:parkings][:service_rest],
            service_atm: params[:parkings][:service_atm],
            service_24_7: params[:parkings][:service_24_7],
            camare_security: params[:parkings][:camare_security]
            )
        @parking.save
    end

    def search
        if params[:home][:search].blank?
            @parkings = Parking.all
        else
            @parkings = Parking.search(params)
        end
    end
  
end