class SessionsController < ApplicationController
    include SessionsHelper
    def new

    end
  
    def create
        puts "CREAR SESSION"
        user = User.find_by(email: params[:session][:email].downcase, password: params[:session][:password])
        
        if user
            puts "ENTRO!"
            log_in user
            redirect_to :users
        else
            flash[:danger] = 'Usuario y/o contraseña no son correctos'
            redirect_to :root
        end
    end
  
    def destroy
        log_out if logged_in?
        redirect_to :root
    end
  
end