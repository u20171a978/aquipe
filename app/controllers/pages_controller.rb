class PagesController < ApplicationController
  def busqueda
  end

  def registrate
  end

  def sesion
  end

  def misestacionamiento
  end

  def reservacion
  end

  def cuenta
  end

  def misreservaciones
  end

  def crearestacionamiento
  end

  def ticket
  end

  def actualizar
  end
  
  def create
  end
end
