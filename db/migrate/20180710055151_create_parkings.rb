class CreateParkings < ActiveRecord::Migration[5.2]
  def change
    create_table :parkings do |t|
      t.string :name
      t.string :address
      t.string :district
      t.string :google_maps
      t.integer :phone
      t.string :pkg_location
      t.string :pkg_place
      t.integer :quantity
      t.integer :price
      t.boolean :service_wash
      t.boolean :service_rest
      t.boolean :service_atm
      t.boolean :service_24_7
      t.boolean :camare_security

      t.timestamps
    end
  end
end
