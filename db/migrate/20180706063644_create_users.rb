class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :type_document
      t.integer :n_document
      t.date :f_birth
      t.integer :phone
      t.string :sex
      t.string :email
      t.string :password
      t.string :remember_token
      t.boolean :tyc
      t.string :type_account

      t.timestamps
    end
  end
end
