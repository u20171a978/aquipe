# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_10_055151) do

  create_table "parkings", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "district"
    t.string "google_maps"
    t.integer "phone"
    t.string "pkg_location"
    t.string "pkg_place"
    t.integer "quantity"
    t.integer "price"
    t.boolean "service_wash"
    t.boolean "service_rest"
    t.boolean "service_atm"
    t.boolean "service_24_7"
    t.boolean "camare_security"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "type_document"
    t.integer "n_document"
    t.date "f_birth"
    t.integer "phone"
    t.string "sex"
    t.string "email"
    t.string "password"
    t.string "remember_token"
    t.boolean "tyc"
    t.string "type_account"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
